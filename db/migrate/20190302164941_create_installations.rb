class CreateInstallations < ActiveRecord::Migration[5.2]
  def change
    create_table :installations do |t|
      t.string :city
      t.string :street
      t.integer :airly_id

      t.timestamps
    end
  end
end
