class CreateMeasurments < ActiveRecord::Migration[5.2]
  def change
    create_table :measurments do |t|
      t.integer :installation_id
      t.float :pm1
      t.float :pm25
      t.float :pm10
      t.float :pressure
      t.float :humidity
      t.float :temperature

      t.timestamps
    end
  end
end
