class CreateReadings < ActiveRecord::Migration[5.2]
  def change
    create_table :readings do |t|
      t.string :batch
      t.json :json

      t.timestamps
    end
  end
end
