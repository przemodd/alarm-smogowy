# README

Hello! That's my first open-source app.
It's based on Ruby on Rails and Leaflet JS map. It will show air pollution based on data from Airly API.

If you are recruiter and want to see some awesome things, you will not find many of them but there's:

* External API consumption

* Scheduled/cron jobs (checking for new sensors every 24 hours and downloading air pollution data every hour)

* Custom rake tasks

* Leaflet JS map with visible pollution (circles with colors depending on pollution)

* ...
