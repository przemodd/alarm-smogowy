class ReadingsController < ApplicationController

  def show
    @reading = Reading.find_by(batch: params[:batch])
    @json = @reading.to_json

    respond_to do |format|
      format.html
      format.json { render json: @reading.json }
    end
  end

end
