module Airly

  require 'rest-client'
  require 'rails/configuration'
  require 'dotenv-rails'

  URL = "https://airapi.airly.eu/v2/"

  LAT = "53.1325"
  LNG = "23.1688"

  def get_installations
    data = JSON.parse( RestClient.get("#{URL}installations/nearest?lat=#{LAT}&lng=#{LNG}&maxDistanceKM=20&maxResults=99", {accept: :json, apikey: AIRLY_KEY}))

    data.each do |installation|
      id = installation["id"]
      if !Installation.exists?(airly_id: id)
        inst = Installation.new
        inst.airly_id = id
        inst.city = installation["address"]["city"]
        inst.street = installation["address"]["street"]

        inst.save!
      end
    end

    def get_measurments
      Installation.all.each do |installation|
        data = JSON.parse(RestClient.get("#{URL}measurements/installation?installationId=#{installation.airly_id}", {accept: :json, apikey: AIRLY_KEY}))

        meas = installation.measurments.new
        meas.pm1 = data["current"]["values"][0]["value"]
        #meas.pm10 = data["current"]["values"][1]["value"]
        #meas.pm25 = data["current"]["values"][2]["value"]
        #meas.pressure = data["current"]["values"][3]["value"]
        #meas.humidity = data["current"]["values"][4]["value"]
        #meas.temperature = data["current"]["values"][5]["value"]
        meas.description = data["current"]['indexes'][0]['description']

        meas.save!
      end
    end
  end
end
