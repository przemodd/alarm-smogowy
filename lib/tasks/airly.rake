require "#{Rails.root}/lib/airly.rb"
include Airly

namespace :airly do
  desc "Get installations from api"
  task get_installations: :environment do
    Airly::get_installations
  end

  desc "Get measurments from api"
  task get_measurments: :environment do
    Airly::get_measurments
  end
end
